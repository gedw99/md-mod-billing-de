package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync/atomic"
	"time"

	"github.com/secsy/goftp"
)

// File is a data struct for all files.
type SourceFile struct {
	Path string `json:"path"`
}

const chunckSize = 64000

func deepCompareFile(file1, file2 string) bool {
	f1s, err := os.Stat(file1)
	if err != nil {
		log.Fatal(err)
	}
	f2s, err := os.Stat(file2)
	if err != nil {
		log.Fatal(err)
	}

	if f1s.Size() != f2s.Size() {
		return false
	}

	f1, err := os.Open(file1)
	if err != nil {
		log.Fatal(err)
	}

	f2, err := os.Open(file2)
	if err != nil {
		log.Fatal(err)
	}

	for {
		b1 := make([]byte, chunckSize)
		_, err1 := f1.Read(b1)

		b2 := make([]byte, chunckSize)
		_, err2 := f2.Read(b2)

		if err1 != nil || err2 != nil {
			if err1 == io.EOF && err2 == io.EOF {
				return true
			} else if err1 == io.EOF && err2 == io.EOF {
				return false
			} else {
				log.Fatal(err1, err2)
			}
		}

		if !bytes.Equal(b1, b2) {
			return false
		}
	}
}

func main() {
	config := goftp.Config{
		ConnectionsPerHost: 1,
		Timeout:            10 * time.Second,
		Logger:             nil,
	}

	// ftp://ftp.kbv.de
	//client, err := goftp.Dial("ftp.kbv.de")
	client, err := goftp.DialConfig(config, "ftp.kbv.de")
	if err != nil {
		panic(err)
	}

	sourceFiles := []SourceFile{}

	Walk(client, "", func(fullPath string, info os.FileInfo, err error) error {
		if err != nil {
			// no permissions is okay, keep walking
			if err.(goftp.Error).Code() == 550 {
				fmt.Println("Failed walking: ", fullPath)
				return err
			}
			return err
			//panic(err)
		}

		fmt.Println(" ")
		fmt.Println("-------------------------")

		isDir := info.IsDir()
		if isDir {
			fmt.Println("Found Dir at: ", fullPath)
		} else {
			fmt.Println("Found File at: ", fullPath)
		}

		// Save into a in memory JSON file, so we can later grab it file and track that we got them.
		if !isDir {
			sourceFileItem := SourceFile{}
			sourceFileItem.Path = fullPath
			//sourceFiles.Append(sourceFiles, sourceFileItem)

		}

		// Convert source remote ftp path into local file system path
		u, err := url.Parse(fullPath)
		if err != nil {
			panic(err)
		}
		currentAbsPath, err := os.Getwd()
		s := []string{currentAbsPath, u.Path}
		localDir := strings.Join(s, "/")

		if isDir {
			// Make Folder to Download into
			fmt.Println("Starting making Dir: ", localDir)
			err := os.MkdirAll(localDir, 0777)
			if err != nil {
				fmt.Println("Failed making Dir: ", localDir, err)
				return err
			}
			fmt.Println("Success making Dir: ", localDir)

			// MUST return here.
			return nil
		}

		// download file
		fmt.Println("Starting downloading File: ", localDir)

		// check if remote is different from local
		localFileInfo, err := os.Stat(localDir)
		if err != nil {
			// no such file or dir
			fmt.Println("Local file does not exist.")
		}
		fmt.Printf("Remote file is %d bytes long. Local File is %d", localFileInfo.Size(), info.Size())

		if localFileInfo.Size() == info.Size() {
			fmt.Println("Skipping because files match. File: ", localDir)
			return nil
		}

		//if info.Size == localFileInfo.Size {
		// }

		// remove files locally that dont exist on remote.
		// only way to do this, is to do it last, by walking remote, making a full list, and then compare to lcal and delete anything local not in remote.

		// Grab it.
		localFile, err := os.Create(localDir)
		if err != nil {
			fmt.Println("Failed to create local file: ", err)
			return err
		}

		err = client.Retrieve(fullPath, localFile)
		if err != nil {
			fmt.Println("Failed downloading File: ", localDir, err)
			return err
		}
		fmt.Println("Success downloading File: ", localDir)

		return nil
	})

}

// Walk a FTP file tree in parallel with prunability and error handling.
// See http://golang.org/pkg/path/filepath/#Walk for interface details.
func Walk(client *goftp.Client, root string, walkFn filepath.WalkFunc) (ret error) {
	dirsToCheck := make(chan string, 100)

	var workCount int32 = 1
	dirsToCheck <- root

	for dir := range dirsToCheck {
		go func(dir string) {
			files, err := client.ReadDir(dir)

			if err != nil {
				if err = walkFn(dir, nil, err); err != nil && err != filepath.SkipDir {
					ret = err
					close(dirsToCheck)
					return
				}
			}

			for _, file := range files {
				if err = walkFn(path.Join(dir, file.Name()), file, nil); err != nil {
					if file.IsDir() && err == filepath.SkipDir {
						continue
					}
					ret = err
					close(dirsToCheck)
					return
				}

				if file.IsDir() {
					atomic.AddInt32(&workCount, 1)
					dirsToCheck <- path.Join(dir, file.Name())
				}
			}

			atomic.AddInt32(&workCount, -1)
			if workCount == 0 {
				close(dirsToCheck)
			}
		}(dir)
	}

	return ret
}
