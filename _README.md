# md-mod-billing-de

This is a module that understands the xDT protocol.

This protocol is used for Medical systems in Germany, and I could not find a golang implementation and so need to implement one.



In terms of test data and code the best I have so far is:  
https://github.com/ncqgm/gnumed/tree/master/gnumed/gnumed/data


In terms of talking with the KV Server the best docs are here:  
http://www.kbv.de/html/ita.php

There is a tool to extract the docs from their server in the docs folder.


## Implemention 

For now we just need code with unit tests to prove things connect and work.
Once we get a feel for things we will then refactor everything over time.
This is a Module and so does not need any GUI, Service API or anything like that.
Just something with units tests so we knwo it all works.

## Roadmap priorities:

The following  order seems to make sense:

- Test data 
    - Gather test data in one place
- golang encoders / decoders
    - Ability to read test data into golang structs 
    - JSON <--> xDT. 
        - JSN is how we store data on disk.
        - xDT is the format that we need to send to KV servers later.

Once we can manipulate data, the next step is to try and use it to interoperate with the KV servers:

- Authentication to KV Servers
    - So we can submit data and get errors back.
    - This requires VPN Software from KV as fas as i know.

- Billing module
    - This is where the doctor sends in his accounting to the insurance company.
    - We can make a simple ledger struct to hold billing data on out side for Testing
    - Then write tests to manipulate it and then send the data to the KV Servers.

If we go this far we are doing well because we know have the encoders and some of the API proxy code working.
From here more parts of the KV API can be implemented as we see fit.





## xDT Protocol notes

xDT is a encoding format for data interchange for the German Health systems.
Its old and prescribed / controlled by the German Health Insurance companies.

Specific higher level implementations for different clases exist on top of xDT.
These are:

ADT (Abrechnungsdatentransfer): A format for transferring billing data
- KVDT is the name of it also because KC control its.
BDT (Behandlungsdatentransfer): A format for exchange of complete electronic health records among electronic health record software systems
GDT (Gerätedatentransfer): A format to transfer data among medical devices and software systems
LDT (Labordatenträger): A format to transfer orders of laboratory tests and their results.



## Refs

Soe of these are not great, but keep all refs we find here just in case we need them latter.


ftp://ftp.kbv.de/ita-update/Abrechnung/KBV_ITA_VGEX_Datensatzbeschreibung_KVDT.pdf
- This is the Offical Documenation. Its heavy i warn you... :)
- In Germny so need to run through Google Translate.

http://wiki.hl7.de/index.php?title=Leitfaden_f%C3%BCr_App-Entwickler#GDT_.2F_XDT
- Kind of useful high level docs.


https://de.wikipedia.org/wiki/XDT#Behandlungsdatentransfer_.28BDT.29
- In Germany xDT format is used for interop.


http://www.kbv.de/html/ita.php/index.html
- KV main repository of Specifications.
- There is allot of stuff here, so its important to be selective.


http://www.qms-standards.de/service/english-summary/
- They are write specs for converting xDT to XML.
- They ae closed source and writing a converter of xDT to XML. For us this is not important.
- But i do know the author and can ask questions once we get to knwo our stuff.


http://wiki.gnumed.de/bin/view/Gnumed/WhatCanIActuallyDOWithGNUmedToday
- gnumed has implemented allot of the xDT higher level protocols in Python and has test data.
- code: https://github.com/ncqgm/gnumed/tree/master/gnumed/gnumed/data

https://github.com/ncqgm/gnumed/blob/master/gnumed/gnumed/client/business/gmXdtMappings.py
- xDT mappings
- they have record numbers to represent field names it seems

https://github.com/FreeMedForms/freemedforms
- might have some there too.



















